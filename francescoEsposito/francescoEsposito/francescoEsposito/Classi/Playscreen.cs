﻿// Classe che mostra l'utilizzo della semantica linguaggio C#
/*using System;
using System.Collections.Generic;
using System.Text;

namespace francescoEsposito.Classi
{
    public class PlayScreen : Screen
    {

        private Manager manager;

        private Bounce game;

        private TextureAtlas atlas;

        private OrthographicCamera gameCam;

        private Viewport gamePort;

        private Hud hud;

        private TiledMap map;

        private OrthogonalTiledMapRenderer mapRenderer;

        private World world;

        private Box2DDebugRenderer b2dr;

        private MovementImpl player;

        public const float GAMEPPM = ((float)(0.4));

        public const float SCALE = 1;

        public const int GRAVITY = 40;

        public const int X_AXIS = 0;

        public const int Y_AXIS = 0;

        public const float FREQUENCY = (((float)(1)) / 60);

        public const int VEL_ITERATION = 1;

        public const int POS_ITERATION = 1;

        public PlayScreen(Bounce game)
        {
            this.game = this.game;
            this.manager = Manager.managerInstance;
            if (this.manager.GetDisableAudio())
            {
                Manager.music.Play();
               
            }

        }

        
        public override void Show()
        {
            this.gameCam = new OrthographicCamera();
            this.gamePort = new FitViewport((Bounce.V_WIDTH * GAMEPPM), (Bounce.V_HEIGHT * GAMEPPM), this.gameCam);
            this.hud = new Hud(this.game.batch);
            map = new TmxMapLoader();
            map = load(("levels/" + ("LVL"
                            + (this.manager.getLevel() + ".tmx")))));
            this.mapRenderer = new OrthogonalTiledMapRenderer(this.map, SCALE);
            this.gameCam.position.set((this.gamePort.getWorldWidth() / 2), (this.gamePort.getWorldHeight() / 2), 0);
            this.world = new World(new Vector2(0, (GRAVITY * -1)), true);
            this.b2dr = new Box2DDebugRenderer();
            this.player = new MovementImpl(new BallImpl(this, X_AXIS, Y_AXIS, this.game), new BallViewImpl());
            this.world.setContactListener(new CollisionImpl());
            ObstaclesCreatorImpl oci = new ObstaclesCreatorImpl(this, this.game);
            oci.createObstacles();
        }

        public void Update(float delta)
        {
            this.world.Step(FREQUENCY, VEL_ITERATION, POS_ITERATION);
            this.hud.Update(delta);
            this.player.Update(delta);
            this.gameCam.position.x = this.player.getBody().getPosition().x;
            if ((this.player.GetBody().GetPosition().y
                        > (this.gamePort.GetWorldHeight() / 2)))
            {
                this.gameCam.position.y = this.player.GetBody().GetPosition().y;
            }

            this.gameCam.Update();
            this.mapRenderer.SetView(this.gameCam);
        }

       
        public override void Dispose()
        {
            this.mapRenderer.Dispose();
            this.map.Dispose();
            this.hud.Dispose();
            this.b2dr.Dispose();
        }

       
        public override void Render(float delta)
        {
            this.Update(delta);
            Gdx.gl.glClearColor(0, 0, 0, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            this.mapRenderer.render(); 
            this.game.batch.SetProjectionMatrix(this.hud.stage.GetCamera().combined);
            this.game.batch.SetProjectionMatrix(this.gameCam.combined);
            this.game.batch.Begin();
            this.player.Draw(this.game.batch);
            this.game.batch.End();
            // chiudiamo la finestra
            this.hud.stage.Draw();
        }

        
        public override void Resize(int width, int height)
        {
            this.gamePort.Update(width, height);
        }

        public TextureAtlas GetAtlas()
        {
            return this.atlas;
        }

        public World GetWorld()
        {
            return this.world;
        }

        public TiledMap GetMap()
        {
            return this.map;
        }

        public OrthographicCamera GetCamera()
        {
            return this.gameCam;
        }

        public MovementImpl GetPlayer()
        {
            return this.player;
        }

        public override void Resume()
        {

        }

        public override void Hide()
        {

        }

        public override void Pause()
        {

        }

        public Hud GetHud()
        {
            return this.hud;
        }
    }
}*/
