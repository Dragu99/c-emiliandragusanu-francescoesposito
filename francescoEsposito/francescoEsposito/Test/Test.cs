﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task;
using System;

namespace Test
{
    [TestClass]
    public class Test
    {
        private PlayScreen playscreen = new PlayScreen();
        private Hud hud = new Hud();

        [TestMethod]
        public void TestPlayScreen()
        {
            Assert.IsNotNull(PlayScreen.VEL_ITERATION);
            Assert.IsNotNull(PlayScreen.POS_ITERATION);
            Assert.AreEqual(1,PlayScreen.POS_ITERATION);
            Assert.AreEqual(1, PlayScreen.VEL_ITERATION);
            Assert.IsNotNull(PlayScreen.FREQUENCY);
            Assert.IsNotNull(playscreen.GetCam());
            Assert.IsNotNull(playscreen.GetMap());
            Assert.IsNotNull(playscreen.GetWorld());
            Assert.IsNotNull(playscreen.GetPlayer());
        }
        [TestMethod]
        public void TestHud()
        {
            Assert.IsNotNull(hud.GetRing());
            Assert.IsNotNull(hud.GetLife());
        }


    }
}
