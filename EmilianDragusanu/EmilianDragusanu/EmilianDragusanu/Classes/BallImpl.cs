﻿using System;
using EmilianDragusanu.Classes;

namespace Classes
{
    public class BallImpl: IBall
    {

        private State currentState;

        private readonly State size;

        private const int X_POS = 128;

        private const int Y_POS = 64;

        private float respawnX = X_POS;

        private float respawnY = Y_POS;

        private const float SIZE = 110;

        private const float SIZE2 = -10000;

        private const float SIZE3 = (float)-0.01;

        private const float SIZE4 = (float)0.01;

        private const float SMALLRADIUS = 11;

        private const float BIGRADIUS = 17;

        private readonly bool rolling;

        private bool jumping = false;

        private bool getBig = false;

        private bool getSmall = false;

        public void SetJumping(bool value)
        {
            this.jumping = value;
        }

        private readonly Hud hud;

        private Bounce game;

        private PlayScreen playscreen;
        private Body body;

        public BallImpl(PlayScreen playscreen, float x, float y, Bounce game)
        {
            this.rolling = false;
            this.getBig = false;
            this.getSmall = true;
            //this.currentState = State.STANDING;
            //this.size = State.SMALL;
            //this.hud = this.playscreen.GetHud();
            //this.SetGame(this.game);
            //this.SetPlayscreen(this.playscreen);
        }

        public Body Body => this.body;


        public bool IsBig()
        {
            if (this.size != State.BIG)
            {
                return false;
            }
            return true;
        }

        public bool IsSmall()
        {
            if (this.size != State.SMALL)
            {
                return false;
            }
            return true;
        }

        public bool IsDead() => false;

        public void SetBig() => this.BigBall();

        private bool BigBall()
        {
            return true;
        }

        public void SetSmall() => this.SmallBall();

        private bool SmallBall()
        {
            return true;
        }

        public void Big()
        {
            this.getBig = true;
            this.getSmall = false;
        }

        public void Small()
        {
            this.getSmall = true;
            this.getBig = false;
        }

        public bool IsRolling => this.rolling;

        public State State => this.currentState;

        public void SetRespawnX(float x) => this.respawnX = x;

        public void SetRespawnY(float y) => this.respawnY = y;

        public PlayScreen Playscreen { get => this.playscreen; set => this.playscreen = value; }

        public Bounce Game { get => this.game; set => this.game = value; }

        public Hud GetHud() => this.hud; 

        public bool IsJumping()
        {
            return this.jumping;
        }

        public Body GetBody() => this.Body;
    }
}

