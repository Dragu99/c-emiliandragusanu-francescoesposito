﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmilianDragusanu.Classes
{
    public class State
    {
        public State()
        {
        }

        public State SMALL { get; internal set; }
        public State BIG { get; internal set; }
    }
}
