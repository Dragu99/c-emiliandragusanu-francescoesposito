﻿using System;
using EmilianDragusanu.Classes;

namespace Classes
{
	public interface IBall
	{
        Body GetBody();

        bool IsBig();

        bool IsSmall();

        bool IsDead();

        void SetBig();

        void SetSmall();

        void SetRespawnX(float x);

        void SetRespawnY(float y);
    }
}
