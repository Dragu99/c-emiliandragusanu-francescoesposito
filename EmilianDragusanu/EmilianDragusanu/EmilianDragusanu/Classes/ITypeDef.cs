﻿using System;
using EmilianDragusanu.Classes;

namespace Classes
{
    public interface ITypeDef
    {

        Body GetBody();

        void SetCategoryFilter(int filterId);

        Fixture GetFixture();
    }
}