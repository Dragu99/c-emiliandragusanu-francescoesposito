﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Classes;
using EmilianDragusanu.Classes;

namespace Tests
{
    [TestClass]
    public class TestBallImpl
    {

        readonly static float x = 10;
        readonly static float y = 10;
        readonly static PlayScreen ps = new PlayScreen();
        readonly static Bounce game = new Bounce();
        readonly static BallImpl ball = new BallImpl(ps, x, y, game);

        [TestMethod]
        public void TestGetter()
        {
            Assert.IsNotNull(ball);
            Assert.IsNull(ball.GetBody());
            Assert.IsNull(ball.GetHud());
        }

    }
}
